# PlantGO

Eine App welche zum gießen von Pflanzen anregt und die Wahrnehmung zur Umwelt steigert.

## Teams

* Backend
    - Algorithmen (Julian, Moritz) 
    - API (Robin)
* App (Onno, Tim, Nicklas)
* Website (Alexander, Gustav, Fanny)

## System

- Flutter
- RestAPI (express.js)
- WebApp

## ToDo

- Buttons und Icons suchen (WebTeam)
Welche Icons genau werden gesucht?
    - [x] Baum
    - [x] Wassereimer
    - [ ] Buttons

    - Websites
        - [Font Awesome Icons Bibliotek](https://fontawesome.com/icons?s=brands)
        - [PNGIMG CreativeCommonsLizenz](https://pngimg.com/)
        - 
    - Bäume
        - Eimer: http://pngimg.com/download/7774
        - Eimer: https://pixabay.com/de/photos/eimer-nass-regen-pflaster-wasser-4335058/
        - Wassertropfen: https://fontawesome.com/icons/tint?style=solid
        - 
    - Eimer
        - Pflanze: https://fontawesome.com/icons/pagelines?style=brands
        - Laubbaum: https://fontawesome.com/icons/tree-alt?style=regular
        - Laubbaum: https://pixabay.com/de/vectors/natur-pflanze-baum-2027899/
        - Tannenbaum: https://fontawesome.com/icons/tree?style=regular
        - 
    - Sonstiges
        - Mappins: https://fontawesome.com/icons?d=gallery&q=map
        - Button: https://pixabay.com/de/vectors/button-klicken-sie-treffen-2155595/
        - Home Button: https://pixabay.com/de/vectors/icon-symbol-gui-haus-home-start-1976100/
        - Settings: https://pixabay.com/de/vectors/getriebe-einstellung-icon-symbol-47203/
        - Search: https://pixabay.com/de/vectors/lupe-suche-suchleiste-finden-icon-1976105/



- [x] Website auf GitLab Hochladen
    - [ ] Link zur Seite?

- [x] Erklärung für die App auf Website

- [ ] ~~Lizenz von Daten Checken~~ Linzenzinformation (CC BY 3.0 `Datenquelle: Stadt Halle (Saale)`) einbinden
    - http://www.halle.de/de/Verwaltung/Online-Angebote/Offene-Verwaltungsdaten/Nutzungsbedingungen-08896/

- [ ] Lizenz an den notwendigen Stellen hinterlegen
- [ ] Wie und Wo soll es für welche Plattform die App zum Download geben?
    - [ ] iOS?
    - [x] Android?
    - [ ] Windows?
- [ ] Wie bekomme ich Punkte?
    - [ ] für Bäume gießen
    - [ ] für Datenbank vervollständigen
    - [ ] für "einnehmen" von Bäumen / Parks 

> [Tim: Ich würde Andriod und IOS nehmen]


## Für die Zukunft
- plausibilität der Gießaktionen prüfen
- Sicherheit
    - Nur 1x pro min. Gießen können.
    - CoCs?
    - Standortüberprüfung
- Stadtkarte mit
    - Bäume
    - Bezirke
    - Wasserquellen
- Gruppen / Clans

- weiter Empfelung ("sharing")
- Lootboxes (kein Pay2Win :D)
- Feedback Function:
    - Feedback zu Baumzustand an das Grünflächenamt (Baum beschädigt, befallen, usw.)  -> Umwelt braucht liebe
    - Welchen Baum sehe ich: Wikidata?


## Extras

Verschwommener Hintergrund für Webseite
>https://www.w3schools.com/howto/howto_css_blurred_background.asp

# Frontend

## Websites

- Onno's Website: https://jugendhackt.gitlab.io/plant-go/backend/index.html
- Tim's Website: http://192.168.21.169:90/ *(Temporär)*


# Backend

## Daten

[Baumkataster](http://www.halle.de/de/Verwaltung/Online-Angebote/Offene-Verwaltungsdaten/Mit-Kartenbezug/index.aspx?ID=f2087a53-2c10-f7c5-4dba-9ad5112a90cb)

Die Daten vom Baumkataster benutzen eine spezielle Projektion der Geokoordinaten.
- Datei ist als `ISO-8859-1` kodiert
- Libary zum Umwandeln von Koordingaten: https://github.com/derhuerst/transform-coordinates
    - Daten liegen in `EPSG:2398` vor wollen aber `EPSG:4326` (WSG 84)
- Libary zum CSV-Parsen: https://github.com/adaltas/node-csv-parse/

## API

### einfache Suche

> http://188.68.40.22:3000/trees/find?type=Laubbaum&year=1968


#### Bäume [trees]

| Key           | Value                |
| ------------- | -------------------- | 
| id            | einzigartige ID      | 
| location_type | Standorttyp          | 
| lat           | Geographische Breite | 
| lon           | Geographische Höhe   | 
| street        | Straßenname          | 
| body_circ     | -                    | 
| body_height   | Baumhöhe             | 
| year          | Pflanzjahr           | 
| height        | Baumhöhe             | 
| type          | Baumart              | 
| specific_type | -                    | 

#### Teams [teams]

| Key           | Value                |
| ------------- | -------------------- | 
| id            | einzigartige ID      |
| name          | Teamname             | 
| tag           | Teamkürzel           | 
| color         | Teamfarbe            | 


#### Teams [teams]

| Key           | Value                |
| ------------- | -------------------- | 
| id            | einzigartige ID      |
| username      | Benutzername         | 
| password      | Passwort             | 
| team          | Team ID              | 

### Bäume in Bereich

> http://188.68.40.22:3000/trees/range?x1=53.123123&y1=23.23423324&x2=52.11122&y2=24.234234


| Key    | Value           |
| ------ | --------------- | 
| x1, y1 | Punkt A (float) | 
| y2, x2 | Punkt B (float) | 


### Gießen als Event

> http://188.68.40.22:3000/trees/irrigation?username=user1&id=1321

| Key      | Value           |
| -------- | --------------- | 
| username | Benutzername    | 
| id       | ID von Baum     | 


### Neuer Benutzer

> http://188.68.40.22:3000/users/create?username=user1&password=(hashed pwd)

| Key      | Value                    |
| -------- | ------------------------ | 
| username | Benutzername             | 
| password | verschlüsseltes Passwort | 

# Präsentation

## Opendata Moritz Julian

- Daten aus einem CSV der Stadt Halle
- Lässt sich sehr schlecht handlen
    - müssen bereinigt / vereinheitlicht werden

## App Fanny

## Website Tim

## Backend Robin

## Logo Lucas
